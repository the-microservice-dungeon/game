package microservice.dungeon.game.health

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.actuate.health.Health
import org.springframework.boot.actuate.health.HealthIndicator
import org.springframework.boot.actuate.health.Status
import org.springframework.kafka.core.KafkaAdminOperations
import org.springframework.stereotype.Component

/**
 * A custom health monitor checking the availability of producer topics.
 *
 * @author Daniel Köllgen
 * @since 03.11.2024
 */
@Component
class KafkaTopicHealthIndicator @Autowired constructor(

    private val admin: KafkaAdminOperations,

    @Value("\${kafka.event.prod.gameStatus.topic}")
    private val gameStatusTopic: String,
    @Value("\${kafka.event.prod.playerStatus.topic}")
    private val playerStatusTopic: String,
    @Value("\${kafka.event.prod.roundStatus.topic}")
    private val roundStatusTopic: String

) : HealthIndicator {

    /**
     * Verifies, that each configured topic has been created with an aggregated health status.
     */
    override fun health(): Health {
        val topics: List<String> = listOf(gameStatusTopic, playerStatusTopic, roundStatusTopic)

        val details: Map<String, Status> = topics.fold(mutableMapOf()) { acc, element ->
            try {
                admin.describeTopics(element)
                acc[element] = Status.UP
            } catch (e: Exception) {
                acc[element] = Status.DOWN
            }
            acc
        }
        val status: Status = details.values.fold(Status.UP) { acc: Status, element: Status ->
            if (acc != Status.UP) {
                acc
            } else {
                element
            }
        }
        return Health.status(status).withDetails(details).build()
    }
}