package microservice.dungeon.game.aggregates.robot.domain

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.Table
import org.hibernate.annotations.JdbcTypeCode
import org.hibernate.type.SqlTypes
import java.util.*

@Entity
@Table(
    name = "robots"
)
class Robot constructor(
    @Id
    @Column(name="robot_id")
    @JdbcTypeCode(SqlTypes.VARCHAR)
    private val robotId: UUID,

    @JdbcTypeCode(SqlTypes.VARCHAR)
    @Column(nullable = false)
    private val playerId: UUID,

    @Column(name = "robot_status")
    private var robotStatus: RobotStatus = RobotStatus.ACTIVE
) {
    fun getRobotId(): UUID = robotId

    fun getPlayerId(): UUID = this.playerId

    fun getRobotStatus(): RobotStatus = robotStatus

    fun destroyRobot() {
        robotStatus = RobotStatus.INACTIVE
    }

    override fun toString(): String {
        return "Robot(robotId=$robotId, playerId=${this.playerId}, robotStatus=$robotStatus)"
    }
}