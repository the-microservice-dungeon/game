package microservice.dungeon.game.aggregates.robot.consumer

import com.fasterxml.jackson.databind.ObjectMapper
import microservice.dungeon.game.aggregates.robot.consumer.dtos.RobotAttackedDto
import microservice.dungeon.game.aggregates.robot.consumer.dtos.RobotSpawnedDto
import microservice.dungeon.game.aggregates.robot.services.RobotService
import mu.KotlinLogging
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.messaging.handler.annotation.Header
import org.springframework.stereotype.Component

/**
 * A kafka consumer listening to robot events.
 */
@Component
class KafkaRobotConsumer @Autowired constructor(
    private val robotService: RobotService,
    private val objectMapper: ObjectMapper
) {

    companion object {
        private val log = KotlinLogging.logger {}
    }

    /**
     * Consumes and processes robots events with to known event types, 'RobotAttacked' and
     * 'RobotSpawned'.
     */
    @KafkaListener(topics = ["robot"])
    fun robotListener(@Header("type") type: String, record: ConsumerRecord<String, String>) {
        when (type) {
            "RobotAttacked" -> handleRobotAttackedEvent(record)
            "RobotSpawned" -> handleRobotSpawned(record)
        }
    }

    private fun handleRobotAttackedEvent(record: ConsumerRecord<String, String>) {
        val event = objectMapper.readValue(record.value(), RobotAttackedDto::class.java)

        log.info("Received 'RobotAttacked' event with payload: {}", event)
        if (!event.attacker.alive) {
            robotService.destroyRobot(event.attacker.robotId)
        }
        if (!event.target.alive) {
            robotService.destroyRobot(event.target.robotId)
        }
    }

    private fun handleRobotSpawned(record: ConsumerRecord<String, String>) {
        val event = objectMapper.readValue(record.value(), RobotSpawnedDto::class.java)

        log.info("Received 'RobotSpawned' Event with payload: {}", event)
        robotService.newRobot(event.robot.id, event.robot.player)
    }
}
