package microservice.dungeon.game.aggregates.robot.services

import jakarta.transaction.Transactional
import microservice.dungeon.game.aggregates.robot.domain.Robot
import microservice.dungeon.game.aggregates.robot.domain.RobotAlreadyExistsException
import microservice.dungeon.game.aggregates.robot.domain.RobotNotFoundException
import microservice.dungeon.game.aggregates.robot.repositories.RobotRepository
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

/**
 * A service responsible for robots.
 */
@Service
class RobotService @Autowired constructor(
    private val robotRepository: RobotRepository
) {
    companion object {
        private val log = KotlinLogging.logger {}
    }

    /**
     * Creates a new robot with the given [robotId] for a player identified by its [playerId].
     */
    @Transactional(rollbackOn = [Exception::class])
    fun newRobot(robotId: UUID, playerId: UUID) {
        if (robotRepository.existsById(robotId)) {
            throw RobotAlreadyExistsException("Failed to create robot '{$robotId}'. Robot already exists.")
        }
        robotRepository.save(Robot(robotId, playerId))
        log.info("New robot '{}' for player '{}' created.", robotId, playerId)
    }

    /**
     * Destroys an existing robot by its [robotId]. Destroyed robots become deactivated.
     */
    @Transactional(rollbackOn = [Exception::class])
    fun destroyRobot(robotId: UUID) {
        val robot = robotRepository.findById(robotId)
            .orElseThrow { RobotNotFoundException("Failed to destroy robot. Robot not found.") }

        robot.destroyRobot()
        robotRepository.save(robot)
        log.info("Robot '{}' destroyed.", robotId)
    }
}
