package microservice.dungeon.game.aggregates.map.domain

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper

/**
 * A configuration regarding the design and layout of game worlds.
 */
data class GameworldSettings (
    val map: MapSettings? = null,
    val resources: ResourceSettings? = null
) {
    /**
     * Serializes into a string.
     *
     * @return the object as string
     */
    fun serialize(): String = jacksonObjectMapper().writeValueAsString(this)

    companion object {
        /**
         * Deserializes the settings from a string. Throws in case the expected mapping is not
         * matched.
         *
         * @return the deserialized game world settings
         */
        fun deserialize(serialized: String?): GameworldSettings = jacksonObjectMapper()
            .readValue(serialized, GameworldSettings::class.java)
    }
}