package microservice.dungeon.game.aggregates.map.domain

import microservice.dungeon.game.aggregates.game.domain.ResourceType
import javax.validation.constraints.Max
import javax.validation.constraints.Min

/**
 * Specifies the resource density by resource type and map area.
 */
data class AreaSettings (
    val inner: Map<ResourceType, @Min(0) @Max(1) Double> = emptyMap(),
    val middle: Map<ResourceType, @Min(0) @Max(1) Double> = emptyMap(),
    val outer: Map<ResourceType, @Min(0) @Max(1) Double> = emptyMap(),
    val border: Map<ResourceType, @Min(0) @Max(1) Double> = emptyMap()
)