package microservice.dungeon.game.aggregates.map.domain

/**
 * Supported map-types for the creation of custom maps by the map-service.
 */
enum class MapType {
    DEFAULT,
    CORRIDOR,
    ISLANDS,
    MAZE,
    CUSTOM,
}
