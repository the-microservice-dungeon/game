package microservice.dungeon.game.aggregates.map.domain

/**
 * Settings specific to the map layout.
 */
data class MapSettings (
    val type: MapType,
    val size: Int? = null,
    val options: Map<String, String> = emptyMap()
)
