package microservice.dungeon.game.aggregates.map.domain

import javax.validation.constraints.Min

/**
 * Settings regarding the resource placement, richness and density.
 */
data class ResourceSettings (
    @Min(1) val minAmount: Int? = null,
    @Min(1) val maxAmount: Int? = null,
    val areaSettings: AreaSettings? = null
)