package microservice.dungeon.game.aggregates.player.controller

import microservice.dungeon.game.aggregates.core.ErrorDetails
import microservice.dungeon.game.aggregates.player.controller.dtos.CreatePlayerRequestDto
import microservice.dungeon.game.aggregates.player.controller.dtos.PlayerResponseDto
import microservice.dungeon.game.aggregates.player.domain.Player
import microservice.dungeon.game.aggregates.player.domain.PlayerAlreadyExistsException
import microservice.dungeon.game.aggregates.player.domain.PlayerNotFoundException
import microservice.dungeon.game.aggregates.player.repository.PlayerRepository
import microservice.dungeon.game.aggregates.player.services.PlayerService
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class PlayerController @Autowired constructor(
    private val playerService: PlayerService,
    private val playerRepository: PlayerRepository
) {
    companion object {
        private val log = KotlinLogging.logger {}
    }

    @ExceptionHandler(value = [PlayerNotFoundException::class])
    fun handlePlayerNotFound(e: PlayerNotFoundException): ResponseEntity<ErrorDetails> {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
            .body(ErrorDetails(e.message, "Check if the provided player is registered."))
    }

    @ExceptionHandler(value = [PlayerAlreadyExistsException::class])
    fun handlePlayerAlreadyExists(e: PlayerAlreadyExistsException): ResponseEntity<ErrorDetails> {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
            .body(ErrorDetails(e.message))
    }

    /**
     * Allows to create a new player by username and mail-address. Both must be unique values.
     */
    @PostMapping("/players", consumes = ["application/json"], produces = ["application/json"])
    fun createNewPlayer(@RequestBody requestPlayer: CreatePlayerRequestDto): ResponseEntity<PlayerResponseDto> {
        log.info("Received a request to create a new player '{}'.", requestPlayer.name)
        val newPlayer = playerService.createNewPlayer(requestPlayer.name, requestPlayer.email)
        val responsePlayer = PlayerResponseDto.makeFromPlayer(newPlayer)

        log.info("Request successful. Responding 201.")
        log.debug("Response: {}", responsePlayer)
        return ResponseEntity(responsePlayer, HttpStatus.CREATED)
    }

    /**
     * Retrieves a player by [username] and [mail]. Both must match.
     */
    @GetMapping("/players", produces = ["application/json"])
    fun getPlayer(
        @RequestParam(name = "name") username: String,
        @RequestParam(name = "mail") mail: String
    ): ResponseEntity<PlayerResponseDto> {
        log.info("Received a request to fetch player '{}' with mail '{}'.", username, mail)

        val player: Player = playerRepository.findByUserNameAndMailAddress(username, mail)
            .orElseThrow { PlayerNotFoundException("Player not found. [playerName=$username, playerMail=$mail]") }
        val responsePlayer = PlayerResponseDto.makeFromPlayer(player)

        log.info("Request successful. Responding 200.")
        log.debug("Response: {}", responsePlayer)
        return ResponseEntity(responsePlayer, HttpStatus.OK)
    }
}
