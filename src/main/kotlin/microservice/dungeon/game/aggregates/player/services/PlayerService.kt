package microservice.dungeon.game.aggregates.player.services

import microservice.dungeon.game.aggregates.player.domain.Player
import microservice.dungeon.game.aggregates.player.domain.PlayerAlreadyExistsException
import microservice.dungeon.game.aggregates.player.repository.PlayerRepository
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * A service responsible for player management.
 */
@Service
class PlayerService @Autowired constructor(
    private val playerRepository: PlayerRepository,
    private val playerExchangeManager: PlayerExchangeManager
) {
    companion object {
        private val log = KotlinLogging.logger {}
    }

    /**
     * Creates a new player with the given [userName] and [mailAddress]. Throws either is already
     * in use.
     */
    @Throws(PlayerAlreadyExistsException::class)
    fun createNewPlayer(userName: String, mailAddress: String): Player {
        if (playerRepository.findByUserNameOrMailAddress(userName, mailAddress).isPresent) {
            throw PlayerAlreadyExistsException("Failed to create player. Username or mail address already in use.")
        }

        val player = Player(userName, mailAddress)
        playerRepository.save(player)
        log.info("New player '{}' created with id '{}'.", player.getUserName(), player.getPlayerId())

        playerExchangeManager.declarePlayerExchange(player)
        return player
    }
}
