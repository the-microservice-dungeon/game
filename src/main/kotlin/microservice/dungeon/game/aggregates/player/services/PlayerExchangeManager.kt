package microservice.dungeon.game.aggregates.player.services

import microservice.dungeon.game.aggregates.player.domain.Player
import mu.KotlinLogging
import org.springframework.amqp.core.*
import org.springframework.amqp.rabbit.core.RabbitAdmin
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * Responsible for managing the player-related queues
 */
@Service
class PlayerExchangeManager @Autowired constructor(
    private val rabbitAdmin: RabbitAdmin
) {

    companion object {
        const val PUBLIC_ROUTING_KEY = "public"
        const val GAME_EXCHANGE_NAME = "game"
        const val PLAYER_ID_HEADER = "playerId"
        private val log = KotlinLogging.logger {}
    }

    private fun buildPlayerNameExchange(player: Player): Exchange {
        return ExchangeBuilder
            .topicExchange(player.getPlayerExchange())
            .build()
    }

    private fun buildPlayerQueue(player: Player): Queue {
        return QueueBuilder
            .durable(player.getPlayerQueue())
            .build()
    }

    private fun buildGameExchange(): Exchange {
        return ExchangeBuilder
            .headersExchange(GAME_EXCHANGE_NAME)
            .build()
    }

    /**
     * Setups the player Queue for a single player.
     */
    fun declarePlayerExchange(player: Player) {
        val playerNameExchange = buildPlayerNameExchange(player)
        val gameExchange = buildGameExchange()

        rabbitAdmin.declareExchange(gameExchange)
        rabbitAdmin.declareExchange(playerNameExchange)

        val playerIdBinding = BindingBuilder
            .bind(playerNameExchange)
            .to(gameExchange)
            .with("IGNORED-PLAYER-ID-BINDING")
            .and(
                mapOf(
                    Pair("x-match", "all"),
                    Pair(PLAYER_ID_HEADER, player.getPlayerId().toString())
                )
            )

        val publicBinding = BindingBuilder
            .bind(playerNameExchange)
            .to(gameExchange)
            .with("IGNORED-PUBLIC-BINDING")
            .and(
                mapOf(
                    Pair("x-match", "all"),
                    Pair(PLAYER_ID_HEADER, PUBLIC_ROUTING_KEY)
                )
            )
        rabbitAdmin.declareBinding(publicBinding)
        rabbitAdmin.declareBinding(playerIdBinding)

        val playerQueue = buildPlayerQueue(player)
        rabbitAdmin.declareQueue(playerQueue)

        val allBinding = BindingBuilder
            .bind(playerQueue)
            .to(playerNameExchange)
            .with("#")
            .noargs()

        rabbitAdmin.declareBinding(allBinding)
    }

    fun setupGameExchange() {
        val publicExchange = buildGameExchange()
        rabbitAdmin.declareExchange(publicExchange)
    }

    fun purgePlayerQueue(player: Player) {
        rabbitAdmin.purgeQueue(player.getPlayerQueue())
    }
}
