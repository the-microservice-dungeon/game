package microservice.dungeon.game.aggregates.player.domain

import jakarta.persistence.*
import org.hibernate.annotations.JdbcTypeCode
import org.hibernate.type.SqlTypes
import java.util.*

@Entity
@Table(
    name = "players",
    uniqueConstraints = [
        UniqueConstraint(name = "player_unique_userName", columnNames = ["user_name"]),
        UniqueConstraint(name = "player_unique_mailAddress", columnNames = ["mail_address"])
    ]
)
class Player constructor(
    @Id
    @Column(name = "player_id")
    @JdbcTypeCode(SqlTypes.VARCHAR)
    private var playerId: UUID,

    @Column(name = "user_name", unique = true)
    private var userName: String,

    @Column(name = "mail_address", unique = true)
    private var mailAddress: String,

    @Column(name = "player_exchange", unique = true)
    private var playerExchange: String,

    @Column(name = "player_queue", unique = true)
    private var playerQueue: String
) {

    constructor(userName: String, mailAddress: String) : this(
        UUID.randomUUID(), userName, mailAddress, "player-${userName}", "player-${userName}"
    )

    fun getPlayerId(): UUID = playerId

    fun getUserName(): String = userName

    fun getMailAddress(): String = mailAddress

    fun getPlayerExchange(): String = playerExchange

    fun getPlayerQueue(): String = playerQueue

    override fun toString(): String {
        return "Player(playerId=$playerId, userName='$userName', mailAddress='${"XXX"}', exchange='$playerExchange', queue='$playerQueue')"
    }
}
