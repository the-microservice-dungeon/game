package microservice.dungeon.game.aggregates.command.controller.dto

import java.util.*

data class SellTradableCommandData(
    val robotId: UUID
) : AbstractCommandData()