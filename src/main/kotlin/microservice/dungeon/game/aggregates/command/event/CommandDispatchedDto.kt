package microservice.dungeon.game.aggregates.command.event

import com.fasterxml.jackson.annotation.JsonUnwrapped
import microservice.dungeon.game.aggregates.command.domain.Command
import microservice.dungeon.game.aggregates.core.EventDto

class CommandDispatchedDto(
    @JsonUnwrapped
    val command: Command
) : EventDto