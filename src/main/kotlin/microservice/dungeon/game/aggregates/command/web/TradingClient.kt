package microservice.dungeon.game.aggregates.command.web

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import microservice.dungeon.game.aggregates.command.web.dto.BuyCommandDto
import microservice.dungeon.game.aggregates.command.web.dto.SellCommandDto
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient

/**
 * A client used to communicate with the trading-service.
 */
@Component
class TradingClient @Autowired constructor(
    @Value(value = "\${rest.trading.baseurl}")
    private val tradingBaseURL: String
) {
    companion object {
        private val logger = KotlinLogging.logger {}
    }

    private val webClient = WebClient.create( if(tradingBaseURL.startsWith("http://")) tradingBaseURL else "http://$tradingBaseURL")

    fun sendSellingCommands(commands: List<SellCommandDto>) {
        if (commands.isEmpty()) {
            logger.trace("No Selling-Commands to dispatch.")
            return
        }
        return try {
            logger.trace("Starting to dispatch {} Selling-Commands to TradingService ...", commands.size)
            transmitCommandsToTrading(commands)
            logger.trace("... dispatching of Selling-Commands completed.")

        } catch (e: Exception) {
            logger.error(e) { "Failed to dispatch Selling-Commands!" }
        }
    }

    fun sendBuyingCommands(commands: List<BuyCommandDto>) {
        if (commands.isEmpty()) {
            logger.trace("No Buying-Commands to dispatch.")
            return
        }
        return try {
            logger.trace("Starting to dispatch {} Buying-Commands to TradingService ...", commands.size)
            transmitCommandsToTrading(commands)
            logger.trace("... dispatching of Buying-Commands completed.")
        } catch (e: Exception) {
            logger.error(e) { "Failed to dispatch Buying-Commands!" }
        }
    }

    private fun transmitCommandsToTrading(commands: List<Any>) {
        logger.trace("Trading-Endpoint is: POST ${tradingBaseURL}/commands")
        logger.trace(ObjectMapper().writeValueAsString(commands))

        webClient.post().uri("/commands")
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .bodyValue(commands)
            .exchangeToMono{ clientResponse ->
                if (clientResponse.statusCode() == HttpStatus.OK) {
                    clientResponse.bodyToMono(JsonNode::class.java)
                }
                else {
                    throw Exception("Connection failed w/ status-code: ${clientResponse.statusCode()}")
                }
            }.block()
    }
}
