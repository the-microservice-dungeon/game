package microservice.dungeon.game.aggregates.command.controller.dto

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import jakarta.validation.constraints.NotNull
import microservice.dungeon.game.aggregates.command.domain.CommandType
import java.util.*

data class CommandWrapperDto(
    @NotNull
    val type: CommandType,

    @NotNull
    val playerId: UUID,

    @JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "type",
        include = JsonTypeInfo.As.EXTERNAL_PROPERTY
    )
    @JsonSubTypes(
        value = [
            JsonSubTypes.Type(value = BattleCommandData::class, name = "battle"),
            JsonSubTypes.Type(value = BuyTradableCommandData::class, name = "buying"),
            JsonSubTypes.Type(value = MineCommandData::class, name = "mining"),
            JsonSubTypes.Type(value = MoveCommandData::class, name = "movement"),
            JsonSubTypes.Type(value = RegenerateCommandData::class, name = "regenerate"),
            JsonSubTypes.Type(value = SellTradableCommandData::class, name = "selling")
        ]
    )
    @NotNull
    val data: AbstractCommandData
)
