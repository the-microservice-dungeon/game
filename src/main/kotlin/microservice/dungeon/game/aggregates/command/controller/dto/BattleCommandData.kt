package microservice.dungeon.game.aggregates.command.controller.dto

import java.util.*

data class BattleCommandData(
    val robotId: UUID,
    val targetId: UUID,
) : AbstractCommandData()