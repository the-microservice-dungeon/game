package microservice.dungeon.game.aggregates.command.web

import com.fasterxml.jackson.databind.ObjectMapper
import microservice.dungeon.game.aggregates.command.web.dto.NewGameWorldDto
import microservice.dungeon.game.aggregates.command.web.dto.NewGameworldResponseDto
import microservice.dungeon.game.aggregates.map.domain.GameworldSettings
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Scope
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient

/**
 * A client used to communicate with the map-service.
 */
@Component
@Scope("singleton")
class MapClient @Autowired constructor(
    @Value(value = "\${rest.map.baseurl}")
    private val mapBaseUrl: String
) {
    companion object {
        private val logger = KotlinLogging.logger {}
    }

    private val webClient = WebClient.create(mapBaseUrl)

    /**
     * Calls the map-service endpoint to attempt the creation of a new game-world.
     *
     * @param numberOfPlayer the number of players
     * @param gameworldSettings optional settings that customize the creation
     * @return the endpoint's response
     *
     * @throws Exception in case the connection fails
     */
    fun createNewGameWorld(numberOfPlayer: Int, gameworldSettings: GameworldSettings?): NewGameworldResponseDto {
        val requestBody = ObjectMapper().writeValueAsString(
            NewGameWorldDto(numberOfPlayer, gameworldSettings)
        )
        logger.info("""
            Connecting to map-service to create a new game world at endpoint '$mapBaseUrl'/gameworlds
            with request-body: '$requestBody'
        """.trimIndent())

        return webClient.post().uri("/gameworlds")
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
            .bodyValue(requestBody)
            .exchangeToMono { clientResponse ->
                if (clientResponse.statusCode() != HttpStatus.OK) {
                    logger.warn("Connection to map-service failed w/ status-code: ${clientResponse.statusCode()}")
                    throw Exception("Connection failed w/ status-code: ${clientResponse.statusCode()}")
                }
                clientResponse.bodyToMono(NewGameworldResponseDto::class.java)
            }.block()!!
    }
}
