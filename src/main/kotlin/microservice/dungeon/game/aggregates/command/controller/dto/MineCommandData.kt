package microservice.dungeon.game.aggregates.command.controller.dto

import java.util.*

data class MineCommandData(
    val robotId: UUID
) : AbstractCommandData()