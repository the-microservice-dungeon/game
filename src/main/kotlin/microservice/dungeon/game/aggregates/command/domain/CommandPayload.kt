package microservice.dungeon.game.aggregates.command.domain

import org.hibernate.annotations.Type
import java.util.*
import jakarta.persistence.Column
import jakarta.persistence.Embeddable
import org.hibernate.annotations.JdbcTypeCode
import org.hibernate.type.SqlTypes

@Embeddable
class CommandPayload(
    @Column(name = "PAYLOAD_PLANET_ID")
    @JdbcTypeCode(SqlTypes.VARCHAR)
    private var planetId: UUID?,

    @Column(name = "PAYLOAD_TARGET_ID")
    @JdbcTypeCode(SqlTypes.VARCHAR)
    private var targetId: UUID?,

    @Column(name = "PAYLOAD_ITEM_NAME")
    private var itemName: String?,

    @Column(name = "PAYLOAD_ITEM_QUANTITY")
    private var itemQuantity: Int?
) {
    fun getPlanetId(): UUID? = planetId

    fun getTargetId(): UUID? = targetId

    fun getItemName(): String? = itemName

    fun getItemQuantity(): Int? = itemQuantity

    override fun toString(): String {
        return "CommandPayload(planetId=$planetId, targetId=$targetId, itemName='$itemName', itemQuantity=$itemQuantity)"
    }
}