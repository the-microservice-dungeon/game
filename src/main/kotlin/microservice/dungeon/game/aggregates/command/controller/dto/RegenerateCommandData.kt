package microservice.dungeon.game.aggregates.command.controller.dto

import java.util.*

data class RegenerateCommandData(
    val robotId: UUID
) : AbstractCommandData()