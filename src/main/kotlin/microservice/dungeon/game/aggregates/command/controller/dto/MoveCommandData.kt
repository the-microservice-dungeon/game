package microservice.dungeon.game.aggregates.command.controller.dto

import java.util.*

data class MoveCommandData(
    val robotId: UUID,
    val planetId: UUID,
) : AbstractCommandData()