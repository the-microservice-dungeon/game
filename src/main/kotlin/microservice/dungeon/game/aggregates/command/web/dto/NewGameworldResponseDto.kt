package microservice.dungeon.game.aggregates.command.web.dto

import java.util.*

data class NewGameworldResponseDto(
    val gameworldId: UUID
)