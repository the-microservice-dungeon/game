package microservice.dungeon.game.aggregates.command.controller.dto

import java.util.*

data class BuyTradableCommandData(
    val robotId: UUID?,
    val itemName: String,
    val itemQuantity: UInt
) : AbstractCommandData()