package microservice.dungeon.game.aggregates.command.controller.dto

import microservice.dungeon.game.aggregates.command.domain.CommandPayload

sealed class AbstractCommandData {
    fun toCommandPayload(): CommandPayload {
        return when (this) {
            is BattleCommandData -> CommandPayload(null, this.targetId, null, null)
            is BuyTradableCommandData -> CommandPayload(
                null,
                null,
                this.itemName,
                this.itemQuantity.toInt()
            )

            is MineCommandData -> CommandPayload(null, null, null, null)
            is MoveCommandData -> CommandPayload(this.planetId, null, null, null)
            is RegenerateCommandData -> CommandPayload(null, null, null, null)
            is SellTradableCommandData -> CommandPayload(null, null, null, null)
        }
    }
}