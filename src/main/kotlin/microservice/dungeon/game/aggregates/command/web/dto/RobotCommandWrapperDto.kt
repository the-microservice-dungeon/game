package microservice.dungeon.game.aggregates.command.web.dto

data class RobotCommandWrapperDto(
    val commands: List<RobotCommandDto>
)