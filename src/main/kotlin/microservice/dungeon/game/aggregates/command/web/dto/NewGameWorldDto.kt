package microservice.dungeon.game.aggregates.command.web.dto

import microservice.dungeon.game.aggregates.map.domain.GameworldSettings

data class NewGameWorldDto(
    val playerAmount: Int,
    val gameworldSettings: GameworldSettings?
)
