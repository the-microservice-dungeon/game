package microservice.dungeon.game.aggregates.command.controller

import jakarta.transaction.Transactional
import jakarta.validation.Valid
import microservice.dungeon.game.aggregates.command.controller.dto.CommandResponseDto
import microservice.dungeon.game.aggregates.command.controller.dto.CommandWrapperDto
import microservice.dungeon.game.aggregates.command.controller.dto.RoundCommandsResponseDto
import microservice.dungeon.game.aggregates.command.domain.Command
import microservice.dungeon.game.aggregates.command.repositories.CommandRepository
import microservice.dungeon.game.aggregates.command.services.CommandService
import microservice.dungeon.game.aggregates.game.domain.RoundNotFoundException
import microservice.dungeon.game.aggregates.game.repositories.RoundRepository
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
class CommandController @Autowired constructor(
    private val commandService: CommandService,
    private val commandRepository: CommandRepository,
    private val roundRepository: RoundRepository,
) {
    companion object {
        private val log = KotlinLogging.logger {}
    }

    @PostMapping("/commands", consumes = ["application/json"], produces = ["application/json"])
    fun createNewCommand(@RequestBody @Valid requestBody: CommandWrapperDto): ResponseEntity<CommandResponseDto> {
        val transactionId = commandService.createNewCommand(
            requestBody.playerId, requestBody.type, requestBody.data
        )
        val responseBody = CommandResponseDto(transactionId)

        return ResponseEntity(responseBody, HttpStatus.CREATED)
    }

    @Transactional(rollbackOn = [Exception::class])
    @GetMapping("/commands", produces = ["application/json"])
    fun getAllRoundCommands(
        @RequestParam(name = "gameId") gameId: UUID,
        @RequestParam(name = "roundNumber") roundNumber: Int
    ):
        ResponseEntity<RoundCommandsResponseDto> {
        val round = roundRepository.findRoundByGame_GameIdAndRoundNumber(gameId, roundNumber)
            .orElseThrow { RoundNotFoundException("Round Number $roundNumber of game '$gameId' not found.") }

        val commands: List<Command> =
            commandRepository.findAllByRoundGameGameIdAndRoundRoundNumber(gameId, roundNumber)
        val responseDto = RoundCommandsResponseDto(round, commands)
        return ResponseEntity(responseDto, HttpStatus.OK)
    }
}
