package microservice.dungeon.game.aggregates.command.controller.dto

import microservice.dungeon.game.aggregates.command.domain.Command
import microservice.dungeon.game.aggregates.command.domain.CommandType
import java.util.*

data class RoundCommandResponseDto(
    val transactionId: UUID,
    val gameId: UUID,
    val playerId: UUID,
    val robotId: UUID?,
    val targetId: UUID?,
    val itemName: String?,
    val itemQuantity: Int?,
    val commandType: CommandType,
) {
    constructor(gameId: UUID, command: Command) : this(
        transactionId = command.getCommandId(),
        gameId = gameId,
        playerId = command.getPlayer().getPlayerId(),
        robotId = command.getRobot()?.getRobotId(),
        targetId = command.getCommandPayload()?.getTargetId(),
        itemName = command.getCommandPayload()?.getItemName(),
        itemQuantity = command.getCommandPayload()?.getItemQuantity(),
        commandType = command.getCommandType(),
    )
}