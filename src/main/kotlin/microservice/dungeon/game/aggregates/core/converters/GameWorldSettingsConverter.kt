package microservice.dungeon.game.aggregates.core.converters

import jakarta.persistence.AttributeConverter
import jakarta.persistence.Converter
import microservice.dungeon.game.aggregates.map.domain.GameworldSettings

@Converter
class GameWorldSettingsConverter : AttributeConverter<GameworldSettings?, String?> {
    override fun convertToDatabaseColumn(attribute: GameworldSettings?): String? {
        if (attribute == null) {
            return null
        }
        return attribute.serialize()
    }

    override fun convertToEntityAttribute(dbData: String?): GameworldSettings? {
        if (dbData.isNullOrEmpty()) {
            return null
        }
        return GameworldSettings.deserialize(dbData)
    }
}