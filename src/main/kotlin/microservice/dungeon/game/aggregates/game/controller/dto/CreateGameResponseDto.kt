package microservice.dungeon.game.aggregates.game.controller.dto

import java.util.*

data class CreateGameResponseDto (
    val gameId: UUID
)