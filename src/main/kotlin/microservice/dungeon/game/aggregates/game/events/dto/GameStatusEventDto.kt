package microservice.dungeon.game.aggregates.game.events.dto

import microservice.dungeon.game.aggregates.core.EventDto
import microservice.dungeon.game.aggregates.game.domain.GameStatus
import java.util.*

class GameStatusEventDto(
    val gameId: UUID,
    val gameworldId: UUID?,
    val status: String
) : EventDto {

    constructor(gameId: UUID, gameworldId: UUID?, gameStatus: GameStatus): this (
        gameId, gameworldId, mapGameStatusToSpecification(gameStatus)
    )

    companion object {
        fun mapGameStatusToSpecification(status: GameStatus): String {
            return when (status) {
                GameStatus.CREATED -> "created"
                GameStatus.GAME_RUNNING -> "started"
                GameStatus.GAME_FINISHED -> "ended"
            }
        }
    }
}