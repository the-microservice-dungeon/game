package microservice.dungeon.game.aggregates.game.domain

enum class ResourceType {
    COAL,
    IRON,
    GEM,
    GOLD,
    PLATIN
}