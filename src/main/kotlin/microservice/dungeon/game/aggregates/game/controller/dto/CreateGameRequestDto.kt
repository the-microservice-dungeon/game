package microservice.dungeon.game.aggregates.game.controller.dto

import jakarta.validation.constraints.Min
import microservice.dungeon.game.aggregates.map.domain.GameworldSettings

/**
 * A DTO responsible for the creation of new games.
 */
data class CreateGameRequestDto (
    @Min(1) val maxPlayers: Int,
    @Min(1) val maxRounds: Int,
    val gameworldSettings: GameworldSettings? = null,
)