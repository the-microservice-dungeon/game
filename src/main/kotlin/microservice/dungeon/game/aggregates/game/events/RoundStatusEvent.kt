package microservice.dungeon.game.aggregates.game.events

import com.fasterxml.jackson.annotation.JsonIgnore
import microservice.dungeon.game.aggregates.core.AbstractEvent
import microservice.dungeon.game.aggregates.game.domain.RoundStatus
import microservice.dungeon.game.aggregates.game.events.dto.RoundStatusEventDto
import microservice.dungeon.game.aggregates.game.services.GameLoopTimingPredictions
import microservice.dungeon.game.aggregates.game.services.GameLoopTimings
import java.time.Instant
import java.util.*

class RoundStatusEvent (
    id: UUID,
    transactionId: UUID,
    occurredAt: Instant,
    eventName: String,
    topic: String,
    version: Int,

    val gameId: UUID,
    val roundId: UUID,
    val roundNumber: Int,
    val roundStatus: RoundStatus,
    val timings: GameLoopTimings,
    val timingPredictions: GameLoopTimingPredictions?

) : AbstractEvent(
    id = id,
    transactionId = transactionId,
    occurredAt = occurredAt,
    eventName = eventName,
    topic = topic,
    version = version
) {
    override fun toDTO(): RoundStatusEventDto = RoundStatusEventDto(gameId, roundId, roundNumber, roundStatus, timings, timingPredictions)
    @JsonIgnore
    override fun getPlayerId(): UUID? = null
}
