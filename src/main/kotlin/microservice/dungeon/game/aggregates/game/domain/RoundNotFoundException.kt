package microservice.dungeon.game.aggregates.game.domain

class RoundNotFoundException(override val message: String) : Exception(message) {
}