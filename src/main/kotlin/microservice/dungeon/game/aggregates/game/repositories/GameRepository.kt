package microservice.dungeon.game.aggregates.game.repositories

import microservice.dungeon.game.aggregates.game.domain.Game
import microservice.dungeon.game.aggregates.game.domain.GameStatus
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import java.util.*

interface GameRepository : CrudRepository<Game, UUID> {
    @Query("SELECT g FROM Game g where g.gameStatus = 'GAME_RUNNING'")
    fun getActiveGame(): Optional<Game>
    fun existsByGameStatusIn(gameStatus: List<GameStatus>): Boolean
    fun findAllByGameStatusIn(gameStatus: List<GameStatus>): List<Game>
}
