package microservice.dungeon.game.aggregates.game.events

import com.fasterxml.jackson.databind.ObjectMapper
import microservice.dungeon.game.aggregates.core.Event
import microservice.dungeon.game.aggregates.core.EventBuilder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import java.time.Instant
import java.util.*

@Component
class PlayerStatusEventBuilder @Autowired constructor(
    @Value("\${kafka.event.prod.playerStatus.topic}")
    private val topic: String,

) : EventBuilder {

    companion object {
        val objectMapper: ObjectMapper = ObjectMapper().findAndRegisterModules()
        const val TYPE = "PlayerStatus"
        const val VERSION = 1
    }

    override fun deserializedEvent(serialized: String): Event {
        return objectMapper.readValue(serialized, PlayerStatusEvent::class.java)
    }

    fun makePlayerStatusEvent(transactionId: UUID, playerId: UUID, gameId: UUID, playerUsername: String): PlayerStatusEvent {
        return PlayerStatusEvent(
            id = UUID.randomUUID(),
            transactionId = transactionId,
            occurredAt = Instant.now(),
            eventName = TYPE,
            topic = topic,
            version = VERSION,
            playerId = playerId,
            playerUsername = playerUsername,
            gameId = gameId
        )
    }
}
