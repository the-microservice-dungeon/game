package microservice.dungeon.game.aggregates.game.controller.dto

import microservice.dungeon.game.aggregates.game.domain.Game
import microservice.dungeon.game.aggregates.game.domain.GameStatus
import microservice.dungeon.game.aggregates.map.domain.GameworldSettings
import java.util.*

data class GameResponseDto (
    val gameId: UUID,
    val gameStatus: String,
    val maxPlayers: Int,
    val maxRounds: Int,
    val currentRoundNumber: Int?,
    val roundLengthInMillis: Long,
    val participatingPlayers: List<String>,
    val gameworldSettings: GameworldSettings?,
) {
    constructor(game: Game): this(
        game.getGameId(),
        mapGameStatusToDtoStatus(game.getGameStatus()),
        game.getMaxPlayers(),
        game.getMaxRounds(),
        game.getCurrentRound()?.getRoundNumber(),
        game.getTotalRoundTimespanInMS(),
        game.getParticipatingPlayers().map { player -> player.getUserName() },
        game.getGameWorldSettings()
    )

    companion object {
        fun mapGameStatusToDtoStatus(gameStatus: GameStatus): String {
            return when(gameStatus) {
                GameStatus.CREATED -> "created"
                GameStatus.GAME_RUNNING -> "started"
                GameStatus.GAME_FINISHED -> "ended"
            }
        }
    }
}
