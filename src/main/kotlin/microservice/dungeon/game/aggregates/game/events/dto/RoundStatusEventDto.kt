package microservice.dungeon.game.aggregates.game.events.dto

import com.fasterxml.jackson.annotation.JsonProperty
import microservice.dungeon.game.aggregates.core.EventDto
import microservice.dungeon.game.aggregates.game.domain.RoundStatus
import microservice.dungeon.game.aggregates.game.services.GameLoopTimingPredictions
import microservice.dungeon.game.aggregates.game.services.GameLoopTimings
import java.util.*

class RoundStatusEventDto (
    val gameId: UUID,
    val roundId: UUID,
    val roundNumber: Int,
    val roundStatus: String,
    @JsonProperty("impreciseTimings")
    val timings: GameLoopTimings,
    @JsonProperty("impreciseTimingPredictions")
    val timingPredictions: GameLoopTimingPredictions?

): EventDto {

    constructor(gameId: UUID, roundId: UUID, roundNumber: Int, roundStatus: RoundStatus, timings: GameLoopTimings, predictions: GameLoopTimingPredictions?):
            this(gameId, roundId, roundNumber, mapStatusToOutputStatus(roundStatus), timings, predictions)

    companion object {
        private fun mapStatusToOutputStatus(status: RoundStatus): String {
            return when (status) {
                RoundStatus.COMMAND_INPUT_STARTED   -> "started"
                RoundStatus.COMMAND_INPUT_ENDED     -> "command input ended"
                RoundStatus.ROUND_ENDED             -> "ended"
                else -> {
                    throw RuntimeException("invalid roundStatus -> event api status mapping")
                }
            }
        }
    }
}