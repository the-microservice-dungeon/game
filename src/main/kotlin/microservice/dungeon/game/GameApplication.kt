package microservice.dungeon.game

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.web.reactive.ReactiveWebServerFactoryAutoConfiguration
import org.springframework.boot.autoconfigure.web.reactive.WebFluxAutoConfiguration
import org.springframework.boot.runApplication
import org.springframework.retry.annotation.EnableRetry

@SpringBootApplication(exclude = [WebFluxAutoConfiguration::class, ReactiveWebServerFactoryAutoConfiguration::class])
@EnableRetry
class GameApplication

fun main(args: Array<String>) {
    runApplication<GameApplication>(*args)
}
