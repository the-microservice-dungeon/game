package microservice.dungeon.game

interface RestProducerContract {

    fun getRequestVerb(): String

    fun getRequestPath(): String

    fun getRequestHttpHeaderContentType(): String

    fun getExpectedResponseCode(): Int

    fun getExpectedResponseBody(): String
}