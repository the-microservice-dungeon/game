package microservice.dungeon.game.aggregates.command.services

import microservice.dungeon.game.AbstractIntegrationTest
import microservice.dungeon.game.aggregates.command.controller.dto.BuyTradableCommandData
import microservice.dungeon.game.aggregates.command.controller.dto.CommandWrapperDto
import microservice.dungeon.game.aggregates.command.controller.dto.SellTradableCommandData
import microservice.dungeon.game.aggregates.command.domain.Command
import microservice.dungeon.game.aggregates.command.domain.CommandPayload
import microservice.dungeon.game.aggregates.command.domain.CommandType
import microservice.dungeon.game.aggregates.command.repositories.CommandRepository
import microservice.dungeon.game.aggregates.eventpublisher.EventPublisher
import microservice.dungeon.game.aggregates.game.domain.Game
import microservice.dungeon.game.aggregates.game.domain.Round
import microservice.dungeon.game.aggregates.game.repositories.GameRepository
import microservice.dungeon.game.aggregates.game.repositories.RoundRepository
import microservice.dungeon.game.aggregates.player.domain.Player
import microservice.dungeon.game.aggregates.player.repository.PlayerRepository
import microservice.dungeon.game.aggregates.robot.domain.Robot
import microservice.dungeon.game.aggregates.robot.repositories.RobotRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.verify
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.annotation.DirtiesContext
import java.util.*

@DirtiesContext
class CommandServiceIntegrationTest @Autowired constructor(
    private val commandService: CommandService,
    private val commandRepository: CommandRepository,
    private val playerRepository: PlayerRepository,
    private val roundRepository: RoundRepository,
    private val gameRepository: GameRepository,
    private val robotRepository: RobotRepository,
    @MockBean private val eventPublisher: EventPublisher
) : AbstractIntegrationTest() {
    private var player: Player? = null
    private var game: Game? = null
    private var round: Round? = null
    private var robot: Robot? = null

    @AfterEach
    fun cleanUp() {
        commandRepository.deleteAll()
        roundRepository.deleteAll()
        gameRepository.deleteAll()
        robotRepository.deleteAll()
        playerRepository.deleteAll()
    }

    @BeforeEach
    fun setUp() {
        player = Player("dadepu", "dadepu@smail.th-koeln.de")
        robot = Robot(UUID.randomUUID(), player!!.getPlayerId())
        playerRepository.save(player!!)
        robotRepository.save(robot!!)

        game = Game(1, 10)
        game!!.joinGame(player!!)
        game!!.startGame()
        game!!.startNewRound()
        round = game!!.getCurrentRound()
        gameRepository.save(game!!)
    }

    @Test
    fun shouldPersistCommandWhenCreatingNewCommand() {
        // given
        val requestBody = CommandWrapperDto(
            CommandType.SELLING,
            playerId = player!!.getPlayerId(),
            SellTradableCommandData(
                UUID.randomUUID()
            )
        )

        // when
        val commandId: UUID = commandService.createNewCommand(
            requestBody.playerId, CommandType.SELLING, requestBody.data
        )

        // then
        val command: Command = commandRepository.findById(commandId).get()
        assertThat(command.getCommandType())
            .isEqualTo(CommandType.SELLING)
        verify(eventPublisher).publishEvent(any())
    }

    @Test
    @Disabled("Don't know if this makes sense")
    fun shouldDeleteDuplicateRobotNulls() {
        // given
        val previousCommand = Command(
            UUID.randomUUID(), round!!, player!!, null, CommandType.BUYING,
            CommandPayload(null, null, "ROBOT", 1)
        )
        commandRepository.save(previousCommand)

        val newCommandRequest = CommandWrapperDto(
            CommandType.SELLING,
            playerId = player!!.getPlayerId(),
            SellTradableCommandData(
                UUID.randomUUID()
            )
        )

        // when
        val commandId: UUID = commandService.createNewCommand(
            newCommandRequest.playerId, CommandType.SELLING, newCommandRequest.data
        )

        // then
        val existsPreviousCommand = commandRepository.existsById(previousCommand.getCommandId())
        val existsNewCommand = commandRepository.existsById(commandId)

        assertThat(existsPreviousCommand)
            .isFalse
        assertThat(existsNewCommand)
            .isTrue
    }

    @Test
    fun shouldUpdateDuplicates() {
        // given
        val previousCommand = Command(
            UUID.randomUUID(), round!!, player!!, robot!!, CommandType.SELLING,
            CommandPayload(null, null, "ROBOT", 1)
        )
        commandRepository.save(previousCommand)

        val newCommandRequest = CommandWrapperDto(
            CommandType.BUYING,
            playerId = player!!.getPlayerId(),
            BuyTradableCommandData(
                null, "ROBOT", 1u
            )
        )

        // when
        val commandId: UUID = commandService.createNewCommand(
            newCommandRequest.playerId, CommandType.BUYING, newCommandRequest.data
        )

        // then
        val command = commandRepository.getById(commandId)
        assertThat(command.getCommandType())
            .isEqualTo(CommandType.BUYING)
    }

    @Test
    fun shouldNotDeleteConsecutiveRobotNullCommands() {
        // given
        val previousCommand = Command(
            UUID.randomUUID(), round!!, player!!, null, CommandType.BUYING,
            CommandPayload(null, null, "ROBOT", 1)
        )
        commandRepository.save(previousCommand)

        val newCommandRequest = CommandWrapperDto(
            CommandType.SELLING,
            playerId = player!!.getPlayerId(),
            BuyTradableCommandData(
                null, "ROBOT", 1u
            )
        )

        // when
        val commandId: UUID = commandService.createNewCommand(
            newCommandRequest.playerId, CommandType.SELLING, newCommandRequest.data
        )

        // then
        val existsPreviousCommand = commandRepository.existsById(previousCommand.getCommandId())
        val existsNewCommand = commandRepository.existsById(commandId)

        assertThat(existsPreviousCommand)
            .isTrue
        assertThat(existsNewCommand)
            .isTrue
    }

    // create new commands
    // remove duplicates
    // find commands by id
}
