package microservice.dungeon.game.aggregates.eventpublisher

import microservice.dungeon.game.aggregates.core.Event
import microservice.dungeon.game.aggregates.eventpublisher.EventPublisher
import microservice.dungeon.game.aggregates.core.KafkaProducing
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.times
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

class EventPublisherTest {
    private var kafkaProducingMock: KafkaProducing? = null
    private var eventPublisher: EventPublisher? = null

    @BeforeEach
    fun setUp() {
        kafkaProducingMock = mock(KafkaProducing::class.java)
        eventPublisher = EventPublisher(kafkaProducingMock!!)
    }

    @Test
    fun shouldAllowToPublishListOfEvents() {
        // given
        val mockEvent: Event = mock()
        val validListOfEvents = listOf(mockEvent, mockEvent)

        // when
        eventPublisher!!.publishEvents(validListOfEvents)

        // then
        verify(kafkaProducingMock!!, times(2)).send(mockEvent)
    }

    @Test
    fun shouldAllowToPublishEvent() {
        // given
        val mockEvent: Event = mock()

        // when
        eventPublisher!!.publishEvent(mockEvent)

        // then
        verify(kafkaProducingMock!!).send(mockEvent)
    }
}
