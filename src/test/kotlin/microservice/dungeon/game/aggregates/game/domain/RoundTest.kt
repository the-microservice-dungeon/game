package microservice.dungeon.game.aggregates.game.domain

import microservice.dungeon.game.aggregates.game.domain.Game
import microservice.dungeon.game.aggregates.game.domain.Round
import microservice.dungeon.game.aggregates.game.domain.RoundStatus
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.EnumSource
import java.time.Instant
import java.time.LocalDateTime
import java.util.*

class RoundTest {
    private val game = Game(10, 100)
    private val someRoundNumber = 3


    @Test
    fun shouldConstructValidStateFromDefaults() {
        val round = Round(game = game, roundNumber = someRoundNumber)

        assertThat(round.getRoundStatus())
            .isEqualTo(RoundStatus.COMMAND_INPUT_STARTED)
        assertThat(round.getRoundStarted())
            .isBeforeOrEqualTo(Instant.now())
    }


    @Test
    fun endCommandInputPhaseShouldSetStatusToCommandInputEnded() {
        val expectedStatus = RoundStatus.COMMAND_INPUT_STARTED
        val round = Round(game = game, roundNumber = someRoundNumber, roundStatus = expectedStatus)
        round.endCommandInputPhase()

        assertThat(round.getRoundStatus())
            .isEqualTo(RoundStatus.COMMAND_INPUT_ENDED)
    }

    @ParameterizedTest
    @EnumSource(
        value = RoundStatus::class,
        names = ["COMMAND_INPUT_STARTED"],
        mode = EnumSource.Mode.EXCLUDE
    )
    fun endCommandInputPhaseShouldThrowWhenStatusIsOtherThanExpected(invalidStatus: RoundStatus) {
        val round = Round(game = game, roundNumber = someRoundNumber, roundStatus = invalidStatus)

        assertThatThrownBy {
            round.endCommandInputPhase()
        }
    }

    @ParameterizedTest
    @EnumSource(RoundStatus::class)
    fun endRoundShouldSetStatusToRoundEnded(status: RoundStatus) {
        val round = Round(game = game, roundNumber = someRoundNumber, roundStatus = status)
        round.endRound()

        assertThat(round.getRoundStatus())
            .isEqualTo(RoundStatus.ROUND_ENDED)
    }

    @ParameterizedTest
    @EnumSource(
        RoundStatus::class,
        names = ["ROUND_ENDED"],
        mode = EnumSource.Mode.EXCLUDE
    )
    fun endRoundShouldShouldBeTrueWhenStatusChangeOccurred(status: RoundStatus) {
        // given
        val round = Round(game = game, roundNumber = someRoundNumber, roundStatus = status)

        // when
        val response = round.endRound()

        // then
        assertThat(response).isTrue

        // when
        val alreadyEndedResponse = round.endRound()

        // then
        assertThat(alreadyEndedResponse).isFalse
    }
}