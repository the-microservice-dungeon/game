package microservice.dungeon.game.aggregates.game.repository

import microservice.dungeon.game.AbstractIntegrationTest
import microservice.dungeon.game.aggregates.game.domain.Game
import microservice.dungeon.game.aggregates.game.domain.GameStatus
import microservice.dungeon.game.aggregates.game.domain.Round
import microservice.dungeon.game.aggregates.game.repositories.GameRepository
import microservice.dungeon.game.aggregates.map.domain.GameworldSettings
import microservice.dungeon.game.aggregates.map.domain.MapSettings
import microservice.dungeon.game.aggregates.map.domain.MapType
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.condition.DisabledIfEnvironmentVariable
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.annotation.DirtiesContext
import java.util.*
@DirtiesContext
class GameRepositoryIntegrationTest @Autowired constructor(
    private val gameRepository: GameRepository
) : AbstractIntegrationTest() {

    @AfterEach
    fun cleanup() {
        gameRepository.deleteAll()
    }

    @Test
    fun shouldAllowToSaveAndFetchGames() {
        // given
        val worldSettings = GameworldSettings(map = MapSettings(type = MapType.ISLANDS))
        val newGame = Game(10, 100, worldSettings)

        // when
        gameRepository.save(newGame)

        // then
        val fetchedGame: Game = gameRepository.findById(newGame.getGameId()).get()
        assertThat(fetchedGame).usingRecursiveComparison().ignoringFields("rounds")
            .isEqualTo(newGame)
    }

    @Test
    fun shouldCascadeAllOnRoundsWhenSaving() {
        // given
        val newGame = Game(10, 100)
        newGame.startGame()

        // when
        gameRepository.save(newGame)

        // then
        val fetchedGame: Game = gameRepository.findById(newGame.getGameId()).get()
        val fetchedActiveRound: Round = fetchedGame.getCurrentRound()!!

        assertThat(fetchedGame.getGameId()).isEqualTo(newGame.getGameId())
        assertThat(fetchedActiveRound.getRoundId()).isEqualTo(newGame.getCurrentRound()!!.getRoundId())
    }

    @Test
    fun shouldFindExistingGameWhenCreatedGameExists() {
        // given
        val newGame = Game(10, 100)
        gameRepository.save(newGame)

        // when
        val response: Boolean = gameRepository.existsByGameStatusIn(listOf(GameStatus.CREATED, GameStatus.GAME_RUNNING))

        // then
        assertTrue(response)
    }

    @Test
    // For whatever reason this tests fails in GitLab CI, but not locally. I have no idea why.
    @DisabledIfEnvironmentVariable(named = "CI", matches = "true")
    fun shouldFindNoExistingGameWhenFinishedGameExists() {
        // given
        val newGame = Game(10, 100)
        newGame.startGame()
        newGame.endGame()
        gameRepository.save(newGame)

        // when
        val response: Boolean = gameRepository.existsByGameStatusIn(listOf(GameStatus.CREATED, GameStatus.GAME_RUNNING))

        // then
        assertFalse(response)
    }

    @Test
    fun shouldNotReturnEndedGame() {
        // given
        val newGame = Game(10, 100)
        newGame.startGame()
        newGame.endGame()
        gameRepository.save(newGame)

        // when
        val response: List<Game> = gameRepository.findAllByGameStatusIn(listOf(GameStatus.CREATED, GameStatus.GAME_RUNNING))

        // then
        assertThat(response)
            .noneMatch { game -> game.getGameStatus() == GameStatus.GAME_FINISHED }
            .noneMatch { game -> game.getGameId() == newGame.getGameId() }
    }

    @Test
    fun shouldFindGamesByStatusIn() {
        // given
        val createdGame = Game(UUID.randomUUID(), null, GameStatus.CREATED, 1, 1, 1, 1, mutableSetOf(), mutableSetOf())
        val startedGame = Game(UUID.randomUUID(), UUID.randomUUID(), GameStatus.CREATED, 1, 1, 1, 1, mutableSetOf(), mutableSetOf())
        startedGame.startGame()
        val finishedGame = Game(UUID.randomUUID(), UUID.randomUUID(), GameStatus.CREATED, 1, 1, 1, 1, mutableSetOf(), mutableSetOf())
        finishedGame.startGame()
        finishedGame.endGame()
        gameRepository.saveAll(listOf(createdGame, startedGame, finishedGame))

        // when
        val fetchedGames: List<UUID> = gameRepository.findAllByGameStatusIn(
            listOf(GameStatus.CREATED, GameStatus.GAME_RUNNING)
        ).map { game -> game.getGameId()}

        // then
        assertThat(fetchedGames)
            .contains(createdGame.getGameId())
        assertThat(fetchedGames)
            .contains(startedGame.getGameId())
        assertThat(fetchedGames)
            .doesNotContain(finishedGame.getGameId())
    }
}