package microservice.dungeon.game.aggregates.player.domain

import microservice.dungeon.game.CustomAssertions.Companion.assertThat
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class PlayerTest {
    private val ANY_USERNAME = "ANY_USERNAME"
    private val ANY_MAILADDRESS = "ANY_MAILADDRESS"


    @Test
    fun shouldInitializeValidObject() {
        // when
        val player = Player(ANY_USERNAME, ANY_MAILADDRESS)

        // then
        assertThat(player).isCreatedFrom(ANY_USERNAME, ANY_MAILADDRESS)
        assertThat(player.getPlayerId()).isNotNull
        assertThat(player.getUserName()).isEqualTo(ANY_USERNAME)
        assertThat(player.getMailAddress()).isEqualTo(ANY_MAILADDRESS)
    }
}
